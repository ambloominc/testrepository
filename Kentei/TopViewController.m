//
//  TopViewController.m
//  Kentei
//
//  Created by 峯香織 on 12/07/19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "TopViewController.h"
#import "QuizViewController.h"

@interface TopViewController ()

@end

@implementation TopViewController

int questionsNum = 0;   // 出題数

#pragma mark - 画面遷移

// 問題数の選択
- (IBAction)toSelect:(id)sender
{
    NSLog(@"%d", [sender tag]);
    NSLog(@"テスト");
    NSLog(@"テスト");
    
    questionsNum = [sender tag];       // 出題数の取得
    [self performSegueWithIdentifier:@"toQuiz" sender:self];    // 出題画面に進む
}

// 画面遷移時のデータの受け渡し
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"toQuiz"])
    {
        QuizViewController *QuizView = (QuizViewController*)[segue destinationViewController];
        QuizView.questionsNum = [NSNumber numberWithInt:questionsNum];  // 出題数の送信
        QuizView.quizCount = [NSNumber numberWithInt:0];                // 問題番号の初期化
        QuizView.trueCount = [NSNumber numberWithInt:0];                // 正解数の初期化
    }
}

// 他のアプリの紹介
- (void)toOtherApp:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"http://itunes.apple.com/jp/artist/ambloom/id499218318"];
    [[UIApplication sharedApplication] openURL:url];
}

#pragma mark- 広告の設定
- (void)SetAd
{
    // i-mobileの広告を表示するビューを作成する(画面下部に配置)
    CGRect frame = CGRectMake(0.0, self.view.frame.size.height-kIMAdViewDefaultHeight, kIMAdViewDefaultWidth, kIMAdViewDefaultHeight);
    IMAdView *imAdView = [[IMAdView alloc] initWithFrame:frame 
                                             publisherId:8261
                                                 mediaId:30971
                                                  spotId:57231];
    
    // i-mobileの広告を表示するビューを、このビューに追加する。
    [self.view addSubview:imAdView];
}

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"トップ画面がよばれました");
    
    [self SetAd];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
