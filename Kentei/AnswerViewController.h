//
//  AnswerViewController.h
//  Kentei
//
//  Created by 峯香織 on 12/07/19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnswerViewController : UIViewController


@property (nonatomic, retain) NSString *answer;         // 正解・不正解の判定
@property (nonatomic, retain) NSNumber *trueNum;        // 正解数
@property (nonatomic, retain) NSNumber *questionNum;    // 出題数
@property (nonatomic, retain) NSNumber *quizCount;      // 問題番号
@property (nonatomic, retain) NSString *answerText;     // 正解分
@property (nonatomic, retain) NSString *comment;        // 解説
@property (nonatomic, retain) IBOutlet UILabel     *answerTextLabel;    // 正解文
@property (nonatomic, retain) IBOutlet UITextView  *commentTextView;    // 解説文
@property (nonatomic, retain) IBOutlet UINavigationItem *navigationItem;     // ナビゲーションバー

- (IBAction)toNext:(id)sender;      // 次の問題へ進む
- (IBAction)AnswerToTop:(id)sender; // トップへ戻る

@end
