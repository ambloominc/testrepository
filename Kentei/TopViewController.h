//
//  TopViewController.h
//  Kentei
//
//  Created by 峯香織 on 12/07/19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <imobileAds/IMAdView.h>

@interface TopViewController : UIViewController


- (IBAction)toSelect:(id)sender;    // 問題の選択
- (IBAction)toOtherApp:(id)sender;  // 他のアプリの紹介

@end
