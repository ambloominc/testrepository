//
//  AnswerViewController.m
//  Kentei
//
//  Created by 峯香織 on 12/07/19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AnswerViewController.h"
#import "ResultViewController.h"
#import "QuizViewController.h"

@interface AnswerViewController ()

@end

@implementation AnswerViewController

@synthesize answer;
@synthesize answerText;
@synthesize comment;
@synthesize answerTextLabel;
@synthesize commentTextView;
@synthesize questionNum;
@synthesize quizCount;
@synthesize navigationItem;
@synthesize trueNum;

#pragma mark - 初期設定

// 表示設定　
-(void)initAnswerView
{
    [self setLabel];    // 表示文章の設定
}

// 文章の設定
- (void)setLabel
{
    // 問題番号
    navigationItem.title = [NSString stringWithFormat:@"第%d問", ([quizCount intValue] + 1)];
    
    // 正解文
    answerTextLabel.text = answerText;                         // 正解文をセット

    // 問題解説
    commentTextView.editable = NO;  // 編集不可
    commentTextView.text = comment; // 解説文を設定
}

#pragma mark - 画面遷移

// 答え合わせ画面に進む
- (IBAction)toNext:(id)sender
{
    NSLog(@"%@",answer);
    
    // 問題番号の設定
    int quizCountInt = [quizCount intValue];            // int型に置き換える
    quizCountInt++;                                     // 問題番号を次に進める
    quizCount = [NSNumber numberWithInt:quizCountInt];  // 送信用にNSNumber型に置き換える
    
    // 問題番号の判定
    if((quizCountInt + 1) > [questionNum intValue]){
        // 最終問題だったら
        NSLog(@"問題終了");
        [self performSegueWithIdentifier:@"toResult" sender:self];  // 結果画面に進む
    }else {
        // まだ問題が残っていたら
        [self performSegueWithIdentifier:@"AnswerToQuiz" sender:self];    // 次の問題へ進む
    }
}

// アラートの表示
- (IBAction)AnswerToTop:(id)sender
{
    // アラートの生成
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"トップへ戻る"
                                                    message:@"問題を終了してトップ画面へ戻りますか？"
                                                   delegate:self
                                          cancelButtonTitle:@"いいえ" 
                                          otherButtonTitles:@"はい", nil];
    [alert show];
}

// アラートの処理
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            NSLog(@"いいえ");
            break;
            
        case 1:
            NSLog(@"はい");
            [self performSegueWithIdentifier:@"AnswerToTop" sender:self];   // トップへ戻る
            
        default:
            break;
    }
}

// 画面遷移時のデータの受け渡し
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // 次の問題
    if([[segue identifier] isEqualToString:@"AnswerToQuiz"]){
        QuizViewController *quizView = (QuizViewController*)[segue destinationViewController];
        quizView.quizCount = quizCount;     // 問題番号
        quizView.trueCount = trueNum;       // 正解数
    }
    // 結果画面
    else if([[segue identifier] isEqualToString:@"toResult"]){
        ResultViewController *resultView = (ResultViewController*)[segue destinationViewController];
        resultView.questionsNum = questionNum;
        resultView.trueCount = trueNum;
    }
}


#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self initAnswerView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
