//
//  CurlDown.m
//  Kentei
//
//  Created by 峯香織 on 12/08/08.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "CurlDown.h"

@implementation CurlDown

- (void)perform
{
    UIViewController *source = (UIViewController *)self.sourceViewController;
    UIViewController *destination = (UIViewController *)self.destinationViewController;
    [UIView transitionWithView:source.navigationController.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCurlDown
                    animations:^{
                        [source.navigationController pushViewController:destination
                                                               animated:NO];
                    }completion:nil];
}

@end
