//
//  CustomSegue.m
//  Kentei
//
//  Created by 峯香織 on 12/07/23.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "CustomSegue.h"

@implementation CustomSegue

- (void)perform
{
    UIViewController *source = (UIViewController *)self.sourceViewController;
    UIViewController *destination = (UIViewController *)self.destinationViewController;
    [UIView transitionWithView:source.navigationController.view
                      duration:0.6
                       options:UIViewAnimationOptionTransitionCurlUp
                    animations:^{
                        [source.navigationController pushViewController:destination
                                                               animated:NO];
                    }completion:nil];
}

@end
