//
//  ResultViewController.h
//  Kentei
//
//  Created by 峯香織 on 12/07/20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController

@property (nonatomic, retain) NSNumber *questionsNum;   // 出題数
@property (nonatomic, retain) NSNumber *trueCount;      // 正解数
@property (nonatomic, retain) IBOutlet UILabel *scoreLabel;         // 結果表示文
@property (nonatomic, retain) IBOutlet UIImageView *resultImage;    // 成績画像

@end

