//
//  ResultViewController.m
//  Kentei
//
//  Created by 峯香織 on 12/07/20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ResultViewController.h"

@interface ResultViewController ()

@end

@implementation ResultViewController

@synthesize questionsNum;
@synthesize trueCount;
@synthesize scoreLabel;
@synthesize resultImage;

#pragma mark - 画面表示
// 表示の設定
- (void)setResultView
{
    scoreLabel.text = [NSString stringWithFormat:@"%d", [trueCount intValue]];           // 得点表示
    [self setImage];
    NSLog(@"正解:%d",[trueCount intValue]);
    NSLog(@"問題:%d",[questionsNum intValue]);
}

// 画像の設定
- (void)setImage
{
    int score = [trueCount intValue];
    
    if(score == 10){
        resultImage.image = [UIImage imageNamed:@"result3.png"];
    }
    else if((score >= 7) && (score < 10)){
        resultImage.image = [UIImage imageNamed:@"result2.png"];
    }
    else if((score >= 4) && (score < 7)){
        resultImage.image = [UIImage imageNamed:@"result1.png"];
    }
    else {
        resultImage.image = [UIImage imageNamed:@"result0.png"];
    }
}

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"結果");
    [self setResultView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
