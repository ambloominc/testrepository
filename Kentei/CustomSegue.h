//
//  CustomSegue.h
//  Kentei
//
//  Created by 峯香織 on 12/07/23.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomSegue : UIStoryboardSegue

- (void)perform;

@end
