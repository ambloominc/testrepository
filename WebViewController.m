//
//  WebViewController.m
//  Kentei
//
//  Created by 峯香織 on 12/08/08.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

#pragma mark - ウェブビュー
- (void) setWebPage
{    
    // webビューの生成
    UIWebView *webview = [[UIWebView alloc] init];
    webview.delegate = self;
    webview.frame = CGRectMake(0.0f, 44.0f, 320.0f, 365.0f);
    webview.scalesPageToFit = NO;
    [self.view addSubview:webview];
    
    // Webページの設定
    NSURL *url = [NSURL URLWithString:@"http://ja.wikipedia.org/wiki/%E8%A5%BF%E9%83%B7%E9%9A%86%E7%9B%9B"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webview loadRequest:request];
    
    // インジケーター
    indicator = [[UIActivityIndicatorView alloc] init];
    indicator.frame = CGRectMake(0.0f, 0.0f, 50.0f, 50.0f);
    indicator.center = self.view.center;
    indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [self.view addSubview:indicator];
}

// 読み込み開始するとインジケータをくるくるまわす
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [indicator startAnimating];
}

// 読み込みが完了するとインジケーターを非表示にする
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [indicator stopAnimating];
}

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setWebPage];
    
    // i-mobileの広告を表示するビューを作成する(画面下部に配置)
    CGRect frame = CGRectMake(0.0, self.view.frame.size.height-kIMAdViewDefaultHeight, kIMAdViewDefaultWidth, kIMAdViewDefaultHeight);
    IMAdView *imAdView = [[IMAdView alloc] initWithFrame:frame 
                                             publisherId:8261
                                                 mediaId:30971
                                                  spotId:57231];
    
    // i-mobileの広告を表示するビューを、このビューに追加する。
    [self.view addSubview:imAdView];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
