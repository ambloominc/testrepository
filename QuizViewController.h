//
//  QuizViewController.h
//  Kentei
//
//  Created by 峯香織 on 12/07/19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <imobileAds/IMAdView.h>

@interface QuizViewController : UIViewController
<
AVAudioPlayerDelegate
>
{
    NSDictionary    *quiz;          // 出題する問題
    NSString        *answer;        // 送信用データ
    NSString        *true_answer;   // 正解
    NSString        *comment_str;   // 解説
    AVAudioPlayer   *player;        // 音声
}

@property (nonatomic, retain) NSNumber *questionsNum;           // 出題数
@property (nonatomic, retain) NSNumber *quizCount;              // 問題番号
@property (nonatomic, retain) NSNumber *trueCount;              // 正解数
@property (nonatomic, retain) IBOutlet UILabel *qustionLabel;   // 問題文
@property (nonatomic, retain) IBOutlet UIButton *choiceButton0; // 選択肢１
@property (nonatomic, retain) IBOutlet UIButton *choiceButton1; // 選択肢２
@property (nonatomic, retain) IBOutlet UIButton *choiceButton2; // 選択肢３
@property (nonatomic, retain) IBOutlet UIButton *choiceButton3; // 選択肢４
@property (nonatomic, retain) IBOutlet UINavigationItem *navigationItem;    //　問題番号


- (IBAction)toAnswer:(id)sender;    // 答え合わせ
- (IBAction)QuizToTop:(id)sender;  // トップへ戻る

@end
