//
//  QuizViewController.m
//  Kentei
//
//  Created by 峯香織 on 12/07/19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "QuizViewController.h"
#import "AnswerViewController.h"

@interface QuizViewController ()

@end

@implementation QuizViewController

@synthesize questionsNum;
@synthesize quizCount;
@synthesize qustionLabel;
@synthesize choiceButton0;
@synthesize choiceButton1;
@synthesize choiceButton2;
@synthesize choiceButton3;
@synthesize navigationItem;
@synthesize trueCount;

NSArray *questions;         // 問題集
NSNumber *questionsNumber;  // 出題数
int allQuiz;                // 総問題数

#pragma mark - プロパティリストの読み込み

// プロパティリストの読み込み

- (NSArray *)loadPlist
{
    // プロパティリストの読み込み
    NSString     *path = [[NSBundle mainBundle] pathForResource:@"Questions" ofType:@"plist"];
    NSDictionary *dic  = [NSDictionary dictionaryWithContentsOfFile:path];
    NSArray      *arr  = [dic objectForKey:@"questions"];
    int          allQuiz = [arr count];      // 総問題数の取得
    
    // 順番を重複しないようにランダムに並べ替え
    srand(time(nil));   // 乱数の値の初期化
    int i, iRand, iBuf;
    int iPut[allQuiz];
    for(i = 0; i < allQuiz; i++){
        iPut[i] = i;
    }
    for(i = 0; i < allQuiz; i++){
        iRand = rand() % allQuiz;
        iBuf = iPut[i];
        iPut[i] = iPut[iRand];
        iPut[iRand] = iBuf;
    }
    
    // 空のリストを作成する
    NSMutableArray *mar = [NSMutableArray array];
    
    // 並べ替えた順番でリストを作成
    for(int j = 0; j < allQuiz; j++){
        [mar addObject:[arr objectAtIndex:iPut[j]]];
    }
    
    return mar;
}

#pragma mark - 表示設定

// 表示の初期設定
- (void)initQuizView
{
    // ボタンの操作の初期化
    choiceButton0.userInteractionEnabled = YES;
    choiceButton1.userInteractionEnabled = YES;
    choiceButton2.userInteractionEnabled = YES;
    choiceButton3.userInteractionEnabled = YES;
    
    // 表示の設定
    [self setQuestion];
    [self setChoiceButtom];
}

// 問題文の設定
- (void)setQuestion
{
    // 出題する問題の要素番号を取り出す
    int quiznum = [quizCount intValue];
    NSLog(@"quiz:%d", quiznum);
    quiz = [questions objectAtIndex:quiznum]; // 問題をセット
    
    // 質問をセット
    NSString *question = [quiz objectForKey:@"question"];
    
    // 問題番号の表示設定
    navigationItem.title = [NSString stringWithFormat:@"第%d問", ([quizCount intValue] + 1)]; // 表示文
    
    // 問題文の表示設定
    qustionLabel.text = question;                           // 問題文をセット
    qustionLabel.lineBreakMode = UILineBreakModeWordWrap;   // 改行モード
    qustionLabel.numberOfLines = 0;                         // 行数制限無し
    [qustionLabel sizeToFit];                               // 自動で高さ調整
    
    true_answer = [quiz objectForKey:@"true"];              // 正解の設定
    comment_str = [quiz objectForKey:@"comment"];           // 解説文
}

// 選択肢の設定
- (void)setChoiceButtom
{
    // ボタンの設定
    NSArray *falseArr = [quiz objectForKey:@"false"];    // 不正解郡の取り出し
    int all = [falseArr count] + 1;                      // 選択肢の数の取得    
     
    // 選択肢の中身の設定
    NSString *choice[all];                          // 選択肢郡
    choice[0] = [quiz objectForKey:@"true"];        // 正解を選択肢の中に追加
    for(int n = 0; n < [falseArr count]; n++){
        choice[n + 1] = [falseArr objectAtIndex:n]; // 不正解郡を選択肢の中に追加
    }
    
    // 選択肢の並び順を重複しないようにランダムに並べ替え
    srand(time(nil));       // 乱数の値の初期化
    int i, iRand, iBuf;
    int iPut[all];
    for(i = 0; i < all; i++){
        iPut[i] = i;
    }
    for(i = 0; i < all; i++){
        iRand = rand() % all;
        iBuf = iPut[i];
        iPut[i] = iPut[iRand];
        iPut[iRand] = iBuf;
    }
    
    // ボタンに選択肢文を表示させる
    // 選択肢の数分だけボタンを表示する
    switch (all) 
    {
        case 0:
            return;
            break;
            
        case 1:
            return;
            break;
            
        case 2:
            [choiceButton0 setTitle:choice[iPut[0]] forState:UIControlStateNormal];
            [choiceButton1 setTitle:choice[iPut[1]] forState:UIControlStateNormal];
            choiceButton2.hidden = YES;
            choiceButton3.hidden = YES;
            break;
            
        case 3:
            [choiceButton0 setTitle:choice[iPut[0]] forState:UIControlStateNormal];
            [choiceButton1 setTitle:choice[iPut[1]] forState:UIControlStateNormal];
            [choiceButton2 setTitle:choice[iPut[2]] forState:UIControlStateNormal];
            choiceButton3.hidden = YES;
            break;
            
        case 4:
            [choiceButton0 setTitle:choice[iPut[0]] forState:UIControlStateNormal];
            [choiceButton1 setTitle:choice[iPut[1]] forState:UIControlStateNormal];
            [choiceButton2 setTitle:choice[iPut[2]] forState:UIControlStateNormal];
            [choiceButton3 setTitle:choice[iPut[3]] forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }
}

#pragma mark - アニメーション

// サウンドの設定
// 再生終了時に呼ばれるメソッド
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    NSLog(@"再生終了で終わり");    
    
    // 答え合わせに進む
    [self performSegueWithIdentifier:@"toAnswer" sender:self];

}

// サウンドの再生
- (void)playSound:(NSString *)sound_name
{
    NSString *path = [[NSBundle mainBundle] pathForResource:sound_name ofType:@"mp3"];
    NSError *error = nil;
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
    
    if(error != nil)
    {
        NSLog(@"AVAudioPlayerのイニシャライズでエラー(%@)", [error localizedDescription]);
        return;
    }
    [player setDelegate:self];
    [player play];
}

// アニメーションの動作
- (void)Animation:(NSString *)image_name:(NSString *)sound_name
{
    // 画像の設定
    UIImage *image = [UIImage imageNamed:image_name];                   // 画像の呼び出し
    UIImageView *animation = [[UIImageView alloc] initWithImage:image]; // ImageViewの設定
    animation.frame = CGRectMake(0.0f, 0.0f, 200.0f, 200.0f);           // 大きさの設定
    animation.center = CGPointMake(self.view.center.x, -100.f);         // 初期位置
    [self.view addSubview:animation];                                   // 画像を画面にセット
    
    // アニメーションの設定
    [UIView beginAnimations:nil context:nil];               // 条件指定開始
    [UIView setAnimationDuration:0.3];                      // アニメーションの動作時間
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];  // アニメーションは一定速度
    animation.center = CGPointMake(self.view.center.x, self.view.center.y); // 画面中央まで画像が移動するアニメーション
    [UIView commitAnimations];                                              // アニメーション開始*/
    
    [self playSound:sound_name];
}


#pragma mark - 答え合わせ

// ボタンタッチ操作
- (IBAction)toAnswer:(id)sender
{
    UILabel *answerLabel = [sender titleLabel]; // 選択肢した答えの取得
    
    // 正解の判定
    BOOL judg = [answerLabel.text isEqualToString:true_answer];
    if(judg == YES){
        // 正解
        answer = @"正解";
        int trueNum = [trueCount intValue] + 1;
        trueCount = [NSNumber numberWithInt:trueNum];
        [self Animation:@"true_image.png":@"sound_true"];
    }else{  
        // 不正解
        answer = @"不正解";
        [self Animation:@"false_image.png":@"sound_false"];
    }
    
    // ボタンを使えなくする
    choiceButton0.userInteractionEnabled = NO;
    choiceButton1.userInteractionEnabled = NO;
    choiceButton2.userInteractionEnabled = NO;
    choiceButton3.userInteractionEnabled = NO;
}

#pragma mark - アラート

// アラートの表示
- (IBAction)QuizToTop:(id)sender
{
    // アラートの生成
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"トップへ戻る"
                                                    message:@"問題を終了してトップ画面へ戻りますか？"
                                                   delegate:self
                                          cancelButtonTitle:@"いいえ" 
                                          otherButtonTitles:@"はい", nil];
    [alert show];
}

// アラート処理
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            NSLog(@"いいえ");
            break;
            
            case 1:
            NSLog(@"はい");
            [self performSegueWithIdentifier:@"QuizToTop" sender:self]; // トップへ戻る
            
        default:
            break;
    }
}

// 画面遷移時のデータの受け渡し
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"toAnswer"]){
        AnswerViewController *AnswerView = (AnswerViewController*)[segue destinationViewController];
        AnswerView.answer = answer;                 // 正解・不正解
        AnswerView.questionNum = questionsNumber;   // 出題数
        AnswerView.quizCount = quizCount;           // 現在の問題番号
        AnswerView.answerText = true_answer;        // 正解文
        AnswerView.comment = comment_str;           // 解説文
        AnswerView.trueNum = trueCount;   // 正解数
    }
}

#pragma mark -広告
- (void)SetAd
{
    // i-mobileの広告を表示するビューを作成する(画面下部に配置)
    CGRect frame = CGRectMake(0.0, self.view.frame.size.height-kIMAdViewDefaultHeight, kIMAdViewDefaultWidth, kIMAdViewDefaultHeight);
    IMAdView *imAdView = [[IMAdView alloc] initWithFrame:frame 
                                             publisherId:8261
                                                 mediaId:30971
                                                  spotId:57231];
    
    // i-mobileの広告を表示するビューを、このビューに追加する。
    [self.view addSubview:imAdView];
}

#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSLog(@"問題画面が呼ばれました");
    if([quizCount intValue] == 0){
        questions = [self loadPlist];   // 問題集を取得
        questionsNumber = questionsNum; // 転送されたデータの格納
    }            
    [self initQuizView];
    [self SetAd];
 }

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
