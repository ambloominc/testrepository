//
//  RankingViewController.h
//  Kentei
//
//  Created by 峯香織 on 12/08/09.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RankingViewController : UIViewController

@property (nonatomic, retain) NSNumber *get_score;
@property (nonatomic, retain) IBOutlet UILabel *resultLabel0;       // 成績
@property (nonatomic, retain) IBOutlet UILabel *resultLabel1;       // 成績
@property (nonatomic, retain) IBOutlet UILabel *resultLabel2;       // 成績
@property (nonatomic, retain) IBOutlet UILabel *resultLabel3;       // 成績
@property (nonatomic, retain) IBOutlet UILabel *resultLabel4;       // 成績

@end
