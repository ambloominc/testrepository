//
//  RankingViewController.m
//  Kentei
//
//  Created by 峯香織 on 12/08/09.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "RankingViewController.h"

@interface RankingViewController ()

@end

@implementation RankingViewController
@synthesize get_score;
@synthesize resultLabel0;
@synthesize resultLabel1;
@synthesize resultLabel2;
@synthesize resultLabel3;
@synthesize resultLabel4;

#pragma mark - 成績表
// 結果の保存
- (NSDictionary *)setResult
{
    // 現在時刻の取得
    NSDateFormatter *date = [[NSDateFormatter alloc]init];
    date.dateFormat = @"yyyy/MM/dd HH:mm";                      // 西暦/月/日 時:分
    NSString *dateStr = [date stringFromDate:[NSDate date]];    // 保存する文字列
    
    // 得点の取得
    NSString *scoreStr = [NSString stringWithFormat:@"%d", [get_score intValue]];          // 保存する文字列
    
    // 保存するデータ
    NSDictionary *date_score = [NSDictionary dictionaryWithObjectsAndKeys:dateStr,@"DATE",
                                scoreStr,@"SCORE",
                                nil];
    
    
    NSLog(@"DATE:%@",[date_score objectForKey:@"DATE"]);
    NSLog(@"SCORE:%@",[date_score objectForKey:@"SCORE"]);
    
    return date_score;
}

// 成績の初期化
- (void)initResult
{
    NSUserDefaults *result = [NSUserDefaults standardUserDefaults]; // 取得
    
    // 初期値の設定
    NSMutableDictionary *defaults = [NSMutableDictionary dictionary];
    NSDictionary *default_score = [NSDictionary dictionaryWithObjectsAndKeys:@"----/--/-- --:--",@"DATE",
                                   @"---",@"SCORE",
                                   nil];
    for(int i = 0; i < 6; i++){
        [defaults setObject:default_score forKey:[NSString stringWithFormat:@"RESULT%d", i]];
    }
    [result registerDefaults:defaults];
}

// 成績の書き込み
- (void)saveResult
{
    [self initResult];
    
    NSUserDefaults *result = [NSUserDefaults standardUserDefaults]; // 取得
    
    // 新規データの保村
    [result setObject:[self setResult] forKey:[NSString stringWithFormat:@"RESULT5"]];  // 新しい成績を確保する
    [result synchronize];                                   // 直ぐに反映させる
    
    NSDictionary *dic[6];   // 成績呼び出し用変数
    
    // 保存されていた成績を呼び出す
    for(int pick = 0; pick < 6; pick++){
        dic[pick] = [result objectForKey:[NSString stringWithFormat:@"RESULT%d", pick]];
    }
    
    // 昇順に並び替え
    NSDictionary *temp; // 入れ替え格納用    
    for(int i = 0; i < 5; i++){
        for(int j = 5; j > i; j--){
            int score1 = [[dic[j] objectForKey:@"SCORE"] intValue];     // 比較する変数１
            int score2 = [[dic[j-1] objectForKey:@"SCORE"] intValue];   // 比較する変数２
            if(score1 > score2){
                temp = dic[j-1];
                dic[j-1] = dic[j];
                dic[j] = temp;            
            }
        }
    }
    
    // 順番を成績に反映
    for(int set = 0; set < 6; set++)
    {
        [result setObject:dic[set] forKey:[NSString stringWithFormat:@"RESULT%d", set]];
        NSLog(@"RESULT%d", set);
        NSLog(@"RESULT%dDATE:%@", set, [dic[set] objectForKey:@"DATE"]);
        NSLog(@"RESULT%dSCORE:%@", set, [dic[set] objectForKey:@"DATE"]);
    }
    
    // 成績を表示する
   // resultLabel0.text = [NSString stringWithFormat:@"１位 %@ %@", [dic[0] objectForKey:@"SCORE"], [dic[0] objectForKey:@"DATE"]];
   // resultLabel1.text = [NSString stringWithFormat:@"２位 %@ %@", [dic[1] objectForKey:@"SCORE"], [dic[1] objectForKey:@"DATE"]];
   // resultLabel2.text = [NSString stringWithFormat:@"３位 %@ %@", [dic[2] objectForKey:@"SCORE"], [dic[2] objectForKey:@"DATE"]];
   // resultLabel3.text = [NSString stringWithFormat:@"４位 %@ %@", [dic[3] objectForKey:@"SCORE"], [dic[3] objectForKey:@"DATE"]];
   // resultLabel4.text = [NSString stringWithFormat:@"５位 %@ %@", [dic[4] objectForKey:@"SCORE"], [dic[4] objectForKey:@"DATE"]];
}


#pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
