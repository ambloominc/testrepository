//
//  main.m
//  test
//
//  Created by 峯香織 on 2012/08/16.
//  Copyright (c) 2012年 峯香織. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
