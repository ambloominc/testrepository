//
//  AppDelegate.h
//  test
//
//  Created by 峯香織 on 2012/08/16.
//  Copyright (c) 2012年 峯香織. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
